/* variables */
// /რა არის ცვლადი (variable)?/;
const x = 10;
const y = 10;
// const z = x + y;
const firstName = "Saba";
// /რა არის მინიჭების ოპერატორი (assignment operator)?/;
let x1 = 10;
console.log((x1 += 5)); // 15
console.log((x1 -= 5)); // 5
console.log((x1 *= 5)); // 50
console.log((x1 **= 5)); // 100
console.log((x1 /= 5)); // 2
// /რა არის მნიშვნელობა (value)?/;
const person = {
  firstName: "saba", // value is saba
  age: 21, // value is 21
  city: "Tbilisi", // value is Tbilisi
};

// /რას ეწოდება კოდის ბლოკი (code block)?/;
if (x === y) {
  // code scope is in {}
  console.log(" 'x' is equal to 'y'");
}
// /რა არის [if | else | else if]?/;
if (x != x1) {
  // if (პირობა)
  console.log(" 'x' is not equal to 'y'");
} else if (x === y) {
  // else if (პირობა 2)
  console.log(" 'x' is equal to 'y'");
} else {
  console.log("we can't help you");
}

// /როგორ მუშაობს: [და, ან, არა] ოპერატორები (and, or, not)?/;
if (x == y && x > y) {
  // && და ოპერატორი რომლის შემთხვევაშიც if-ის ორივე პირობა უნდა შესრულდეს, ამ შემთხვევაში პირველი პირობა იქნება უარყოფითი
  console.log("IT IS CORRECT");
} else {
  console.log("XXX");
}
if (x == y || x > y) {
  // ||- ან ოპერატორი , რომლის შემთხვევაშიც ერთ-ერთი პრიობა მაინც უნდა შესრულდეს.
  console.log("IT IS CORRECT");
} else {
  console.log("XXX");
}
// /როგორ მუშაობს switch (case და default)?/;
let dayList = "monday";
switch (dayList) {
  case "monday":
    console.log("working");
    break;
  case "tuesday":
    console.log("training");
    break;
  case "wednesday":
    console.log("swimming");
    break;
  case "thursday":
    console.log("playing");
    break;
  case "friday":
    console.log("singing");
    break;
  case "saturday":
    console.log("running");
    break;
  case "sunday":
    console.log("relaxing");
    break;
  default:
    console.log("sleeping");
    break;
}
// /ახსენით რა არის და რისთვის გამოიყენება მონაცემთა სტრუქტურები (data structures)?/;
// data structures - sring , boolean , number

// /რა არის მასივი (array)?/;
const cars = ["bmw", "ferrari", "ford"]; // array - არის როდესაც გვაქვს 1 -ზე მეტი მნიშვნელობა.

// /როგორ ხდება მასივის მანიპულირება [წვდომა, წაშლა, ჩამატება, შეცვლა]?/;
cars.push("porshche", "kia"); // push - ჩამატება
delete cars[1]; // delete წაშლა
console.log(cars);

// /რა არის ციკლი?/
// /როგორ მუშაობს for ციკლი?/;
// /რა არის იტერაცია იგივე ბიჯი?/;
for (let i = 0; i < cars.length; i++) {
  console.log(cars[i]);
}

// /როგორ მუშაობს while ციკლი?/;
let weather = "raining";

while ((weather = true)) {
  // console.log(" there is raining");
}
// /როგორ მუშაობს do while ციკლი?/;;
let b = 10;
do {
  console.log(" hello ");
} while ((b < 15, b++));

// /რა სხვაობაა [break -ს და continue -ს] შორის?/;
const numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
for (let nums of numbers) {
  if (nums == "6") break; // დაიბეჭდება ყველა რიცხვი 6 -ამდე .
  console.log(nums);
}
for (let nums of numbers) {
  if (nums == "6") continue; // continue -ს შემთხვევაში რიცხვი 6 არ დაიბეჭდება
  console.log(nums);
}

// /რა არის ფუნქცია და როგორ მუშაობს ის?/;
function meet(name) {
  console.log(`hello ${name}!`);
}
meet("gio");
meet("saba");

// /რა არის ფუნქციის ტანი (function body)?/;
// ფუნქციის ტანი არის მოთავსებული ფიგურულ ფრჩხიილებს შორის {}

// /რას ნიშნავს ფუნქციის განსაზღვრა (definition)?/;
// /რა არის ფუნქციის გამოძახება (call, invoke)?/;
const person1 = {
  firstName: "Saba",
  lastName: "Orkodashvili",
  fullName: function () {
    return this.firstName + " " + this.lastName;
  },
};
console.log(person1.fullName());
// /რა არის პარამეტრები?/;
// პარამეტრი არის რაც function () ფრჩხილებში იწერება

// /რა არის default პარამეტრი?/;
// /რა უნდა გავითვალისწინოთ default პარამეტრის გამოყენებისას?/;

// /რას ნიშნაბს ფუნქციის დასაბრუნებელი მნიშვნელობა (return value)?/;
// /რას ნიშნაბს ფუნქციის დასაბრუნებელი მნიშვნელობა (return value)?/;
// returt value ნიშნავს იმას თუ რა უნდა დაგვიწეროს მოცემულმა ფუნქციამ

// /ახსენით ხილვადობები (local scope vs global)/;
// ფუნქციაში ჩაწერილი ცვლადი არ იქნება ხელმისაწვდომი ფუნქციის გარეთ ამას ეწოდება local scope  ხოლო global ცვლადი ყველგან იქნება ხელმისაწვდომი

// /რა არის shadowing?/;
// shadowing  არის local scope- ში შექმნილ ცვლადს როდესაც ეცვლება მნიშვნელობა და ხდება გადაჩრდილვა.

// /რა არის რეკურსია?/;
// რეკურსია არის ფუნქციის გამოძახება

// /რა არის ობიექტი (object)?/;
// object - არის ისეთი ცვლადი რომელშიც ინახები თვისება და თვისების მნიშვნელობა, მაგალითისთვის შეგვიძლია ავიღოთ მანქანა და მისი თვისებები
const car = {
  name: "ferrari",
  country: "italy",
  Founder: "Enzo Ferrari",
};

// /არის თუ არა object-ი მონაცემთა სტრუქტურა?/;
//კი არის

// /რა არის properties?/;
products = ["product-1", "product-2", "product-3", "product-4"]; // products- ში მოთავსებული მნიშვნელობები არის properties

// /რა არის მეთოდი და რითი განსხვავდება ფუნქციისგან?/;
const person3 = {
  firstName: "Saba",
  lastName: "orkodashvili",
  fullName() {
    // მეთოდი არის ობიექტში მოთავსებული ფუნქცია
    return `${this.firstName} ${this.lastName}`;
  },
};

// /როგორ მუშაობს this?/;
// this უნდა მივწვდეთ ობიექტში არსებულ მნიშვნელობებს
